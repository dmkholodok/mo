import numpy as np
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings('ignore')


def calculate_hypotesis(x, weights):
    return x.dot(weights)


def cost_func(x, y, weights):
    h = calculate_hypotesis(x, weights)
    error_pow2 = (h - y) ** 2
    return np.mean(error_pow2) / 2


def normalize_features(x):
    feature_count = x.shape[1]
    copy_x = x.copy()
    for i in range(feature_count):
        feature = x.values[:, i]
        mean = np.mean(feature)
        delta = np.max(feature) - np.min(feature)
        copy_x.values[:, i] -= mean
        copy_x.values[:, i] /= delta
    return copy_x


def normal_equation(x, y):
    x = x.astype('float64')
    y = y.astype('float64')

    row_count = x.shape[0]
    x = np.column_stack((np.ones(row_count), x))

    return np.linalg.inv(x.T.dot(x)).dot(x.T).dot(y)


class LinearRegressionMultVars:

    THRESHOLD = 1

    def __init__(self, max_steps=100000, linear_rate=0.01, normalize=True, vectorize=True):
        self.normalize = normalize
        self.vectorize = vectorize
        self.max_steps = max_steps
        self.linear_rate = linear_rate
        self.weights = None
        self.costs = []

    def fit(self, x, y):
        x = x.astype('float64')
        y = y.astype('float64')

        if self.normalize:
            x = normalize_features(x)

        row_count = x.shape[0]
        x = np.column_stack((np.ones(row_count), x))

        feature_count = x.shape[1]
        self.weights = np.zeros(feature_count)

        cur_loss = float('inf')
        cur_step = 1
        while cur_step < self.max_steps:
            loss = cost_func(x, y, self.weights)
            self.costs.append(loss)
            if abs(loss - cur_loss) < self.THRESHOLD:
                break

            self.gradient_descent(x, y)

            cur_loss = loss
            cur_step += 1

    def gradient_descent(self, x, y):
        h = calculate_hypotesis(x, self.weights)
        error = h - y
        if self.vectorize:
            self._vectorized_gradient_descent(x, error)
        else:
            self._simple_gradient_descent(x, error)

    def _simple_gradient_descent(self, x, error):
        feature_count = x.shape[1]
        for j in range(feature_count):
            self.weights[j] -= self.linear_rate * np.mean(x[:, j] * error)

    def _vectorized_gradient_descent(self, x, error):
        gradient = np.dot(x.T, error)
        row_count = x.shape[0]
        gradient /= row_count
        gradient *= self.linear_rate
        self.weights -= gradient


FILENAME2 = 'ex1data2.txt'
df = pd.read_csv(FILENAME2, header=None, names=['area', 'rooms', 'price'])
x_train, y_train = df.filter(['area', 'rooms']), df['price']
print(df)

not_normalized = LinearRegressionMultVars(max_steps=100, normalize=False)
not_normalized.fit(x_train, y_train)
xi_nn, costs_non_normalized = list(range(not_normalized.costs.__len__())), not_normalized.costs

normalized = LinearRegressionMultVars(max_steps=100, normalize=True)
normalized.fit(x_train, y_train)
xi_norm, costs_normalized = list(range(normalized.costs.__len__())), normalized.costs

fig, ax1 = plt.subplots()
ax1.plot(xi_nn, costs_non_normalized)
ax1.set_xlabel('Number of steps')
ax1.set_ylabel('Cost function')
ax1.set_title('Not normalized')

fig2, ax2 = plt.subplots()
ax2.plot(xi_norm, costs_normalized)
ax2.set_xlabel('Number of steps')
ax2.set_ylabel('Cost function')
ax2.set_title('Normalized')
plt.show()

not_vectorized = LinearRegressionMultVars(max_steps=10000, linear_rate=0.005, vectorize=False)
vectorized = LinearRegressionMultVars(max_steps=10000, linear_rate=0.005, vectorize=True)

start_time = datetime.now()
not_vectorized.fit(x_train, y_train)
end_time = datetime.now()
not_vectorized_fit_time = end_time - start_time

start_time = datetime.now()
vectorized.fit(x_train, y_train)
end_time = datetime.now()
vectorized_fit_time = end_time - start_time

print("Fit time with vectorization (microseconds):    {}".format(vectorized_fit_time.microseconds))
print("Fit time without vectorization (microseconds): {}".format(not_vectorized_fit_time.microseconds))

linearRegression1 = LinearRegressionMultVars(linear_rate=0.01)
linearRegression1.fit(x_train, y_train)
xi1, costs1 = list(range(linearRegression1.costs.__len__())), linearRegression1.costs
fig, ax = plt.subplots()
ax.plot(xi1, costs1)
ax.set_xlabel('Number of steps')
ax.set_ylabel('Cost function')
ax.set_title('Linear rate = 0.01')

linearRegression2 = LinearRegressionMultVars(linear_rate=0.1)
linearRegression2.fit(x_train, y_train)
xi2, costs2 = list(range(linearRegression2.costs.__len__())), linearRegression2.costs
fig, ax2 = plt.subplots()
ax2.plot(xi2, costs2)
ax2.set_xlabel('Number of steps')
ax2.set_ylabel('Cost function')
ax2.set_title('Linear rate = 0.1')

plt.show()

weights = normal_equation(x_train, y_train)
single_vector_x_train = np.column_stack((np.ones(x_train.shape[0]), x_train))
normal_equation_cost = cost_func(single_vector_x_train, y_train, weights)

linearRegression = LinearRegressionMultVars(linear_rate=0.1)
linearRegression.fit(x_train, y_train)
gradient_descent_cost = linearRegression.costs[-1]

print("Cost function using analytical approach: {}".format(normal_equation_cost))
print("Cost function using gradient descent:    {}".format(gradient_descent_cost))