from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import pandas as pd
from matplotlib import cm
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings('ignore')


def loss_func(x, y, theta_0, theta_1):
    hypotesis = calculate_hypotises(x, theta_0, theta_1)
    coef = 1. / (2 * y.size)
    return coef * np.sum((hypotesis - y) ** 2)


def calculate_hypotises(x, theta_0, theta_1):
    A = np.column_stack((np.ones(x.size), x))
    b = np.array([theta_0, theta_1])
    return A.dot(b)


class LinearRegressionOneVar:

    def __init__(self, max_steps=1000, linear_rate=0.01):
        self.theta_0 = 0
        self.theta_1 = 1
        self.linear_rate = linear_rate
        self.max_steps = max_steps
        self.__logs = []

    def predict(self, x):
        return self.theta_0 + self.theta_1 * x

    def fit(self, x, y):
        x_values = x.values
        y_values = y.values

        curr_step = 0
        for curr_step in range(self.max_steps):
            loss = loss_func(x_values, y_values, self.theta_0, self.theta_1)
            if curr_step % 10 == 0:
                self.__logs.append([curr_step, self.theta_0, self.theta_1, loss])

            self.gradient_descent(x_values, y_values)

    def gradient_descent(self, x, y):
        hypotesis = calculate_hypotises(x, self.theta_0, self.theta_1)
        y_size = y.size
        errors = hypotesis - y
        theta_0 = self.theta_0 - self.linear_rate * np.sum(errors) / y_size
        theta_1 = self.theta_1 - self.linear_rate * np.sum(np.multiply(errors, x)) / y_size
        self.theta_0, self.theta_1 = theta_0, theta_1

    @property
    def logs(self):
        return pd.DataFrame(self.__logs, columns=['iter', 'theta_0', 'theta_1', 'loss'])




FILENAME = 'ex1data1.txt'
df = pd.read_csv(FILENAME, header=None, names=['population', 'profit'])
x_train, y_train = df['population'], df['profit']
print(df)

plt.plot(x_train, y_train, 'o', markersize=4)
plt.xlabel('population')
plt.ylabel('profit')
plt.show()

lin = LinearRegressionOneVar(max_steps=1000, linear_rate=0.01)
lin.fit(x_train, y_train)
min_x, max_x = int(min(x_train)), int(max(x_train))
xi = list(range(min_x, max_x + 1))
line = [lin.predict(i) for i in xi]
print("Function: y = {} + {} * x".format(lin.theta_0, lin.theta_1))

plt.plot(x_train, y_train, 'o', xi, line, markersize=4)
plt.xlabel('population')
plt.ylabel('profit')
plt.show()

data = lin.logs
data = data[data['theta_1'] > 1]

X, Y = np.meshgrid(data['theta_0'], data['theta_1'])
Z = np.zeros((data['theta_0'].size, data['theta_1'].size))

for i, x in enumerate(data['theta_0']):
    for j, y in enumerate(data['theta_1']):
        Z[i, j] = loss_func(x_train, y_train, x, y)

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=1)
ax.set_xlabel('Theta 0')
ax.set_ylabel('Theta 1')
ax.set_zlabel('Loss')
plt.show()

fig, ax = plt.subplots()
CS = ax.contour(X, Y, Z, cmap='viridis')
ax.clabel(CS, inline=True, fontsize=10)
ax.set_title('Countour plot')
ax.set_xlabel('Theta 0')
ax.set_ylabel('Theta 1')
plt.show()
