import logistic_regression as lr
import numpy as np


class MultiLogisticRegression:
    classifier = lr.LogisticRegression

    def __init__(self, classes_count=10):
        self.classes_count = classes_count
        self.classifiers = [
            self.classifier(fit_method='bfgs', regularized=True, regularization_param_value=0.1)
            for i in range(classes_count)
        ]

    def fit(self, x, y):
        for i in range(self.classes_count):
            _y_train = (y == i).astype(int)
            self.classifiers[i].fit(x, _y_train)

    def predict(self, x):
        h = []
        for cls in self.classifiers:
            h.append(lr.calculate_hypotesis(x, cls.weights))

        return np.argmax(np.array(h), axis=0)

