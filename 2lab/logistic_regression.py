import numpy as np
from scipy.optimize import fmin
from scipy.optimize import fmin_bfgs


def cost_func_not_regularized(x, y, weights, t=0):
    predictions = calculate_hypotesis(x, weights)
    cost_true = y * np.log(predictions)
    cost_false = (1 - y) * np.log(1 - predictions)
    return -np.mean(cost_true + cost_false)


def gradient_not_regularized(x, y, weights, t=0):
    predictions = calculate_hypotesis(x, weights)
    error = predictions - y
    gradient = np.dot(x.T, error)
    return gradient / x.shape[0]


def gradient_regularized(x, y, weights, regularization_param_value):
    predictions = calculate_hypotesis(x, weights)
    error = predictions - y
    gradient_one = np.dot(x.T[:1], error)
    gradient_two_and_more = np.dot(x.T[1:], error) + regularization_param_value * weights[1:]
    gradient = np.insert(gradient_two_and_more, 0, gradient_one)
    return gradient / x.shape[0]


def cost_func_regularized(x, y, weights, regularization_param_value):
    cost = cost_func_not_regularized(x, y, weights)
    return cost + (regularization_param_value / (2 * x.shape[0])) * np.dot(weights.T, weights)


def calculate_hypotesis(x, weights):
    return sigmoid(x.dot(weights))


def sigmoid(z):
    return 1 / (1 + np.e ** (-z))


class LogisticRegression:
    THRESHOLD = 1e-6
    MAX_STEP_COUNT = 300000

    def __init__(self,
                 learning_rate=0.01,
                 regularized=False,
                 regularization_param_value=0.5,
                 fit_method='gradient_descent',
                 max_steps=MAX_STEP_COUNT,
                 log=False):
        self.max_steps = max_steps
        self.learning_rate = learning_rate
        self.regularized = regularized
        self.costs = []
        self.weights = []
        self.regularization_param_value = regularization_param_value
        self.cost_func = cost_func_regularized if regularized else cost_func_not_regularized
        self.gradient_func = gradient_regularized if regularized else gradient_not_regularized
        self.fit_impl = getattr(self, fit_method)
        self.log = log

    def fit(self, x, y):
        if hasattr(x, 'values'):
            x = x.values
        if hasattr(y, 'values'):
            y = y.values

        x = x.astype('float64')
        y = y.astype('float64')

        if not self.regularized:
            x = np.column_stack((np.ones(x.shape[0]), x))

        self.fit_impl(x, y)

    def gradient_descent(self, x, y):
        self.weights = np.zeros(x.shape[1])

        cur_step = 0
        cur_loss = self.cost_func(x, y, self.weights, self.regularization_param_value)
        while cur_step < self.max_steps:
            cur_step += 1
            self.gradient_descent_step(x, y, self.weights)
            loss = self.cost_func(x, y, self.weights, self.regularization_param_value)
            self.costs.append(loss)
            if abs(loss - cur_loss) < self.THRESHOLD:
                break

            cur_loss = loss

    def gradient_descent_step(self, x, y, weights):
        gradient = self.gradient_func(x, y, weights, self.regularization_param_value)
        gradient *= self.learning_rate
        self.weights -= gradient

    def nelder_mead(self, x, y):
        def cost_func(theta):
            return self.cost_func(x, y, theta, self.regularization_param_value)

        self.weights = fmin(cost_func, np.zeros(x.shape[1]), xtol=self.THRESHOLD, maxfun=self.MAX_STEP_COUNT)

    def bfgs(self, x, y):
        def cost_func(theta):
            return self.cost_func(x, y, theta, self.regularization_param_value)

        def gradient_func(theta):
            return self.gradient_func(x, y, theta, self.regularization_param_value)

        self.weights = fmin_bfgs(cost_func, np.zeros(x.shape[1]), fprime=gradient_func, gtol=self.THRESHOLD, disp=self.log)

    def predict(self, x):
        x = np.array(x)
        if not self.regularized:
            x = np.insert(x, 0, 1)
        h = calculate_hypotesis(x, self.weights)
        return 1 if h >= 0.5 else 0
