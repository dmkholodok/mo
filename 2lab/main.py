import pandas as pd
import matplotlib.pyplot as plt
import logistic_regression as lr
import multi_logistic_regression as mlr
import numpy as np
from scipy.io import loadmat


def decision_boundary_contour(df, theta1, theta2, theta3):
    x_split = np.linspace(-1, 1.2, 50)
    y_split = np.linspace(-1, 1.2, 50)

    def calc_z(theta):
        z = np.zeros(shape=(len(x_split), len(y_split)))
        for i in range(len(x_split)):
            for j in range(len(y_split)):
                z[i, j] = build_poly_features(np.array(x_split[i]), np.array(y_split[j])).dot(theta)
        return z.T

    z1 = calc_z(theta1)
    z2 = calc_z(theta2)
    z3 = calc_z(theta3)

    fig, ax_reg = plt.subplots()
    ax_reg.legend(loc='upper right')
    ax_reg.set_xlabel('First test')
    ax_reg.set_ylabel('Second test')
    ax_reg.contour(x_split, y_split, z1, levels=0, colors='b')
    ax_reg.contour(x_split, y_split, z2, levels=0, colors='g')
    ax_reg.contour(x_split, y_split, z3, levels=0, colors='y')

    passed = df[df['passed'] == 1]
    not_passed = df[df['passed'] == 0]

    ax_reg.scatter(passed['first_test'], passed['second_test'], marker='o', c='black', label='Passed', s=20)
    ax_reg.scatter(not_passed['first_test'], not_passed['second_test'], marker='x', c='red', label='Not passed', s=20)
    plt.show()


def build_poly_features(x1, x2, log=False):
    degree = 6
    res = []
    str_res = []

    for i in range(degree + 1):
        for j in range(i, degree + 1):
            res.append(x1**(j - i) * x2**i)
            first = '' if j - i == 0 else 'x1' if j - i == 1 else f'x1^{j - i}'
            second = '' if i == 0 else 'x2' if i == 1 else f'x2^{i}'
            if not first and not second:
                str_append = '1'
            elif first and not second:
                str_append = first
            elif second and not first:
                str_append = second
            else:
                str_append = f"{first}*{second}"
            str_res.append(str_append)

    str_res = ' + '.join(str_res)
    if log:
        print(str_res)
    return np.array(res).T


def decision_boundary(x, weights):
    return -(weights[0] + weights[1] * x) / weights[2]


# 1
df = pd.read_csv('ex2data1.txt', header=None, names=['first_exam', 'second_exam', 'accepted'])

# 2
accepted = df[df['accepted'] == 1]
not_accepted = df[df['accepted'] == 0]
fig, ax = plt.subplots()
ax.scatter(accepted['first_exam'], accepted['second_exam'], marker='o', c='red', label='Accepted', s=20)
ax.scatter(not_accepted['first_exam'], not_accepted['second_exam'], marker='x', c='black', label='Not accepted', s=20)
ax.legend(loc='upper right')
ax.set_xlabel('First exam')
ax.set_ylabel('Second exam')
plt.show()

# 3
x_train, y_train = df.filter(['first_exam', 'second_exam']), df['accepted']
logistic_regression = lr.LogisticRegression(learning_rate=0.004)
logistic_regression.fit(x_train, y_train)
print(f'Minimized cost function value: {logistic_regression.costs[-1]}')
print(f'Iterations count: {len(logistic_regression.costs)}')
print(f'Weights: {logistic_regression.weights}')

# 4
nm_lr = lr.LogisticRegression(fit_method='nelder_mead')
nm_lr.fit(x_train, y_train)
print('Weights: {}'.format(nm_lr.weights))
bfgs_lr = lr.LogisticRegression(fit_method='bfgs', log=True)
bfgs_lr.fit(x_train, y_train)
print('Weights: {}'.format(bfgs_lr.weights))


# 6
z_true = df[df['accepted'] == 1]
z_false = df[df['accepted'] == 0]
fig, ax = plt.subplots()
ax.set_xlabel('First exam')
ax.set_ylabel('Second exam')
ax.scatter(z_true['first_exam'], z_true['second_exam'], marker='o', c='red', label='Accepted', s=20)
ax.scatter(z_false['first_exam'], z_false['second_exam'], marker='x', c='black', label='Not accepted', s=20)
ax.plot(z_false['first_exam'], [decision_boundary(i, logistic_regression.weights) for i in z_false['first_exam']], c='blue', label='Decision boundary')
ax.legend(loc='upper right')
plt.show()

# 7
df = pd.read_csv('ex2data2.txt', header=None, names=['first_test', 'second_test', 'passed'])
x_train, y_train = df.filter(['first_test', 'second_test']), df['passed']

# 8
passed = df[df['passed'] == 1]
not_passed = df[df['passed'] == 0]
fig, ax_reg = plt.subplots()
ax_reg.scatter(passed['first_test'], passed['second_test'], marker='o', c='red', label='Passed', s=20)
ax_reg.scatter(not_passed['first_test'], not_passed['second_test'], marker='x', c='black', label='Not passed', s=20)
ax_reg.legend(loc='upper right')
ax_reg.set_xlabel('First test')
ax_reg.set_ylabel('Second test')
plt.show()

# 9
x_poly = build_poly_features(x_train['first_test'], x_train['second_test'], log=True)

# 10
reg_lr = lr.LogisticRegression(regularized=True, learning_rate=0.5, regularization_param_value=0.5)
reg_lr.fit(x_poly, y_train)
print(f'Minimized cost function value: {reg_lr.costs[-1]}')
print(f'Iterations count: {len(reg_lr.costs)}')
print(f'Weights: {reg_lr.weights}')

# 11
reg_nm_lr = lr.LogisticRegression(fit_method='nelder_mead', regularized=True)
reg_nm_lr.fit(x_poly, y_train)
print(f'Weights: {reg_nm_lr.weights}')

reg_bfgs_lr = lr.LogisticRegression(fit_method='bfgs', regularized=True, log=True)
reg_bfgs_lr.fit(x_poly, y_train)
print(f'Weights: {reg_bfgs_lr.weights}')

# 12
print(f"Predicted class: {reg_lr.predict(x_poly[0])}, actual class: {y_train[0]}")
print(f"Predicted class: {reg_nm_lr.predict(x_poly[0])}, actual class: {y_train[0]}")
print(f"Predicted class: {reg_bfgs_lr.predict(x_poly[0])}, actual class: {y_train[0]}")

# 13
decision_boundary_contour(df, reg_lr.weights, reg_nm_lr.weights, reg_bfgs_lr.weights)

# 14
reg_lr1 = lr.LogisticRegression(learning_rate=0.5, regularized=True, regularization_param_value=0.5)
reg_lr2 = lr.LogisticRegression(learning_rate=0.5, regularized=True, regularization_param_value=0.05)
reg_lr3 = lr.LogisticRegression(learning_rate=0.5, regularized=True, regularization_param_value=0.005)
reg_lr1.fit(x_poly, y_train)
reg_lr2.fit(x_poly, y_train)
reg_lr3.fit(x_poly, y_train)
decision_boundary_contour(df, reg_lr1.weights, reg_lr2.weights, reg_lr3.weights)

# 15
mat = loadmat('ex2data3.mat')
x_train, y_train = mat['X'], mat['y']
y_train = y_train.reshape(y_train.shape[0])
y_train = np.where(y_train != 10, y_train, 0)


# 16
def vector_to_matrix(x):
    vector_length = len(x)
    step = int(np.sqrt(vector_length))
    matrix = [x[left:left+step] for left in range(0, vector_length, step)]
    np_matrix = np.array(matrix).T
    reversed_matrix = np.flip(np_matrix, axis=0)
    return reversed_matrix


nums = list(range(1, 5000, 500))
pictures = [vector_to_matrix(x_train[i]) for i in nums]

fig, axs = plt.subplots(2, 5, figsize=(14, 6))
for i, ax in enumerate(axs.flatten()):
    ax.pcolor(pictures[i], cmap='binary')
plt.show()

# 20
mult_lr = mlr.MultiLogisticRegression()
mult_lr.fit(x_train, y_train)
value = mult_lr.predict(x_train[0])
print(f"Predicted class: {value}, actual class: {y_train[0]}")


def accuracy(cls, x, y):
    error = cls.predict(x) - y
    return 1.0 - (float(np.count_nonzero(error)) / len(error))


acc = accuracy(mult_lr, x_train, y_train)
print(f"Accuracy: {acc}")

