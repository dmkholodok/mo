import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
import linear_regression as lr


def reshape(arr):
    return arr.reshape(arr.shape[0])


def poly_features(X, p=8):
    x = X.reshape(X.shape[0])
    res = np.array(x)

    for i in range(2, p + 1):
        res = np.column_stack((res, x ** i))

    return res


def normalize_features(X):
    n = X.shape[1]
    copy_X = X.copy()
    for i in range(n):
        feature = X[:, i]
        mean = np.mean(feature)
        delta = np.max(feature) - np.min(feature)
        copy_X[:, i] -= mean
        copy_X[:, i] /= delta
    return copy_X


def predict_and_show(model, X, y):
    min_x, max_x = int(min(X)), int(max(X))
    x = list(range(min_x - 1, max_x + 1))
    y_predicted = [model.predict(np.array(i)) for i in x]
    plt.plot(X, y, 'o', x, y_predicted, markersize=4)
    plt.xlabel('Water level change')
    plt.ylabel('Water volume')
    plt.show()


def predict_and_show_poly(model, X, y):
    x = np.linspace(min(X), max(X), 1000)
    x_poly_norm = normalize_features(poly_features(x))
    y_predicted = [model.predict(i) for i in x_poly_norm]
    plt.plot(X, y, 'o', x, y_predicted, markersize=4)
    plt.xlabel('Water level change')
    plt.ylabel('Water volume')
    plt.show()


def learning_curves_two(model, X_train, y_train, X_val, y_val, max_axis=50):
    plt.xlabel("Number of training examples")
    plt.ylabel("Error")

    n_train, n_val = len(y_train), len(y_val)
    train_err, val_err = np.zeros(n_train), np.zeros(n_val)

    for i in range(1, n_train + 1):
        model.fit(X_train[0:i], y_train[0:i])
        train_err[i-1] = model.costs[-1]

    for i in range(1, n_val + 1):
        model.fit(X_val[0:i], y_val[0:i])
        val_err[i-1] = model.costs[-1]

    plt.plot(range(1, n_train + 1), train_err, c="black", linewidth=2)
    plt.plot(range(1, n_val + 1), val_err, c="red", linewidth=2)
    plt.legend(["Training", "Validation"], loc="best")
    plt.axis([0, np.max([n_train, n_val]) + 1, 0, max_axis])
    plt.show()


def learning_curves_one(model, X, y, max_axis=50):
    plt.xlabel("Number of training examples")
    plt.ylabel("Error")

    n_train = len(y)
    err = np.zeros(n_train)

    for i in range(1, n_train + 1):
        _x = X[0:i]
        _y = y[0:i]
        model.fit(_x, _y)
        err[i-1] = model.costs[-1]

    plt.plot(range(1, n_train + 1), err, c="black", linewidth=2)
    plt.axis([0, np.max(n_train) + 1, 0, max_axis])
    plt.show()


# 1
mat = loadmat('ex3data1.mat')
X_train, y_train = mat['X'], reshape(mat['y'])
X_val, y_val = mat['Xval'], reshape(mat['yval'])
X_test, y_test = mat['Xtest'], reshape(mat['ytest'])

# 2
plt.plot(X_train, y_train, 'o', markersize=4)
plt.xlabel('Water level change')
plt.ylabel('Water volume')
plt.show()

# 5
model = lr.LinearRegression(learning_rate=0.001, regularization_param_value=0)
model.fit(X_train, y_train)
predict_and_show(model, X_train, y_train)

# 6
model = lr.LinearRegression(learning_rate=0.001, regularization_param_value=0)
learning_curves_two(model, X_train, y_train, X_val, y_val)

# 10
X_train_poly_norm = normalize_features(poly_features(X_train))
X_val_poly_norm = normalize_features(poly_features(X_val))
model = lr.LinearRegression(learning_rate=0.001, regularization_param_value=0)
model.fit(X_train_poly_norm, y_train)
predict_and_show_poly(model, X_train, y_train)
learning_curves_two(model, X_train_poly_norm, y_train, X_val_poly_norm, y_val)

# 11
model = lr.LinearRegression(learning_rate=0.001, regularization_param_value=1)
model.fit(X_train_poly_norm, y_train)
predict_and_show_poly(model, X_train, y_train)
learning_curves_two(model, X_train_poly_norm, y_train, X_val_poly_norm, y_val, max_axis=200)

model = lr.LinearRegression(learning_rate=0.001, regularization_param_value=100)
model.fit(X_train_poly_norm, y_train)
predict_and_show_poly(model, X_train, y_train)
learning_curves_two(model, X_train_poly_norm, y_train, X_val_poly_norm, y_val, max_axis=200)

# 12
plt.xlabel("Lambda")
plt.ylabel("Error")
plt.title("Validation Curve")

learning_rate = 0.5
lambda_values = np.arange(0, 0.4, 0.005)
validation_errors = []

for l in lambda_values:
    model = lr.LinearRegression(learning_rate=learning_rate, regularization_param_value=l)
    model.fit(X_train_poly_norm, y_train)
    val_cost = lr.regularized_cost_func(X_val_poly_norm, y_val, model.weights, l)
    validation_errors.append(val_cost)

lambda_index = np.argmin(np.array(validation_errors))
lambda_res = lambda_values[lambda_index]
print(f"Lambda = {lambda_res}")

plt.plot(lambda_values, validation_errors)
plt.axis([0, max(lambda_values), 0, max(validation_errors)])
plt.show()

model = lr.LinearRegression(learning_rate=learning_rate, regularization_param_value=lambda_res)
model.fit(X_train_poly_norm, y_train)
X_test_poly_norm = normalize_features(poly_features(X_test))
print(f"Error on test set is {lr.regularized_cost_func(X_test_poly_norm, y_test, model.weights, lambda_res)}")
