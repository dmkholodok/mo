import numpy as np


def regularization(regularization_param_value, theta):
    return regularization_param_value * np.dot(theta.T, theta)


def regularized_cost_func(x, y, weights, regularization_param_value):
    predictions = calculate_hypotesis(x, weights)
    error_pow_two = (predictions - y) ** 2
    regularization_value = regularization(regularization_param_value, weights[1:])
    return (error_pow_two.sum() + regularization_value) / (2 * x.shape[0])


def regularized_gradient(x, y, weights, regularization_param_value):
    predictions = calculate_hypotesis(x, weights)
    error_pow_two = predictions - y
    gradient_one = np.dot(x.T[:1], error_pow_two)
    gradient_two_and_more = np.dot(x.T[1:], error_pow_two) + regularization_param_value * weights[1:]
    gradient = np.insert(gradient_two_and_more, 0, gradient_one)
    return gradient / x.shape[0]


def calculate_hypotesis(X, weights):
    if len(X.shape) > 1 and X.shape[1] < weights.shape[0]:
        X = np.column_stack((np.ones(X.shape[0]), X))
    return X.dot(weights)


class LinearRegression:
    THRESHOLD = 1e-6
    MAX_STEP_COUNT = 500000

    def __init__(self,
                 learning_rate=0.01,
                 regularized=False,
                 regularization_param_value=0.5,
                 max_steps=MAX_STEP_COUNT):
        self.max_steps = max_steps
        self.learning_rate = learning_rate
        self.regularized = regularized
        self.costs = []
        self.weights = []
        self.regularization_param_value = regularization_param_value

    def fit(self, x, y):
        x = x.astype('float64')
        y = y.astype('float64')

        x = np.column_stack((np.ones(x.shape[0]), x))
        self.gradient_descent(x, y)

    def gradient_descent(self, x, y):
        self.weights = np.zeros(x.shape[1])
        self.costs = []
        cur_step = 0
        cur_loss = regularized_cost_func(x, y, self.weights, self.regularization_param_value)
        self.costs.append(cur_loss)
        while cur_step < self.max_steps:
            cur_step += 1
            gradient = regularized_gradient(x, y, self.weights, self.regularization_param_value)
            self.weights -= self.learning_rate * gradient
            loss = regularized_cost_func(x, y, self.weights, self.regularization_param_value)
            self.costs.append(loss)
            if abs(loss - cur_loss) < self.THRESHOLD:
                break

            cur_loss = loss

    def predict(self, x):
        X = np.insert(x, 0, 1)
        return calculate_hypotesis(X, self.weights)
